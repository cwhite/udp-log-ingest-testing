#!/bin/bash

if [ ! -d '/tmp/rendered_logs' ]; then
  mkdir /tmp/rendered_logs
fi

set -xe

podman run -it --rm --net host -v /tmp/rendered_logs:/srv/mw-log:Z wikimedia_udplog:latest
