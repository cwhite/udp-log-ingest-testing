UDP Ingest Stress Testing
===

This test suite was created to gather performance data for https://phabricator.wikimedia.org/T353936.

Usage
---
1. Build and run the desired ingest tool.
2. Run `stress_test.py`

Assumptions
---
 * Ingest tools expect to write the rendered logs at `/tmp/rendered_logs`
 * Ingest tools expect to listen on UDP port 8420.
