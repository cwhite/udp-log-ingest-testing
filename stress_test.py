#!/usr/bin/env python3
"""
Python script to spam logs for profiling udp log receivers
"""
import socket
import time
import json
import sys
import os

# sample logs to spam into the log receiver
# must be in udplog expected format without newline character termination
# e.g. <type> <log line>
EXAMPLE_LOGS = [
    'xff 2023-12-21 17:36:09.167725 [6d2cdf10-a027-11ee-acc3-b79636bc219c] mw2354 enwiki 1.42.0-wmf.9 xff INFO: Thu, 21 Dec 2023 17:36:09 +0000.https://en.wikipedia.org/w/api.php.0.0.0.0, 0.0.0.0, 0.0.0.0. {"date":"Thu, 21 Dec 2023 17:36:09 +0000","uri":"https://en.wikipedia.org/w/api.php","xff":"0.0.0.0, 0.0.0.0","remoteaddr":"0.0.0.0","wpSave":""}',
    'CirrusSearch 2023-12-21 17:37:48.896564 [481dc731-9596-4e6b-a60a-750d60332db7] mw2281 mgwiktionary 1.42.0-wmf.10 CirrusSearch DEBUG: Running sendData on cluster codfw 0s after insertion {"method":"sendData","arguments":["content",[{"Elastica\\Document":[]}]],"diff":0,"cluster":"codfw"}',
]

# how many events to spam into the log receiver
MAX_EVENTS = 1e6  # one million

# log receiver listening address and port
TARGET = ('127.0.0.1', 8420)

# how log to sleep prior to gathering statistics about the run - allow the receiver to finish its batching and flush
SLEEP_DURATION = 3

# throttle towards emitting this many events per second
# <=0 or >= MAX_EVENTS disables the throttler
TARGET_EVENTS_PER_SECOND = 0

# expected rmem_max value
RMEM_MAX = 536870912

# expected rmem_default value
RMEM_DEFAULT = 4194304

# where will we find the rendered logs?
RENDERED_FILE_PATH = os.path.join(os.sep, 'tmp', 'rendered_logs')


def check_net_settings() -> None:
    expected_rmem_max = RMEM_MAX
    expected_rmem_default = RMEM_DEFAULT
    with open('/proc/sys/net/core/rmem_max', 'r') as f:
        actual_rmem_max = int(f.read())
    with open('/proc/sys/net/core/rmem_default', 'r') as f:
        actual_rmem_default = int(f.read())
    try:
        assert(actual_rmem_max == expected_rmem_max)
        assert(actual_rmem_default == expected_rmem_default)
    except AssertionError:
        print(f'rmem values not like mwlog.  Please run:\nsudo sysctl -w net.core.rmem_default={expected_rmem_default}\nsudo sysctl -w net.core.rmem_max={expected_rmem_max}', file=sys.stderr)
        sys.exit(1)


def get_udp_stats() -> dict:
    udp_data_raw = []
    udp_data_parsed = {}
    with open('/proc/net/snmp', 'r') as f:
        snmp_data = f.read()

    for line in snmp_data.split('\n'):
        if line.startswith('Udp:'):
            udp_data_raw.append(line)

    for i, key in enumerate(udp_data_raw[0].replace('Udp: ', '').split(' ')):
        udp_data_parsed[key] = int(udp_data_raw[1].replace('Udp: ', '').split(' ')[i])

    return udp_data_parsed


def sleep_until(timestamp_ns) -> None:
    now = time.perf_counter_ns()
    while now < timestamp_ns:
        now = time.perf_counter_ns()


def calculate_emit_window() -> int:
    if TARGET_EVENTS_PER_SECOND <= 0 or TARGET_EVENTS_PER_SECOND >= MAX_EVENTS:
        return -1
    return int(1e9 / TARGET_EVENTS_PER_SECOND)


def emit_events() -> list:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    events_emitted = 0
    log_idx = 0
    log_max = len(EXAMPLE_LOGS) - 1
    counts = [0 for _ in range(0, len(EXAMPLE_LOGS))]

    # throttler
    # 1. we have divided up the second into emit_window chunks
    emit_window = calculate_emit_window()  # nanoseconds between windows
    # 2. capture the timestamp of the first window
    window_start = time.perf_counter_ns()

    # 3. begin emitting
    while events_emitted < MAX_EVENTS:
        events_emitted += 1
        counts[log_idx] += 1
        sock.sendto(bytes(f'{EXAMPLE_LOGS[log_idx]}\n', 'utf-8'), TARGET)
        log_idx += 1
        if log_idx > log_max:
            log_idx = 0

        # 4. move window_start to the time of the next window_start
        window_start += emit_window
        # 5. check the time in a tight loop until now is greater than the new window start
        sleep_until(window_start)

    sock.close()
    return counts


def count_rendered_logs(log_type) -> int:
    with open(os.path.join(RENDERED_FILE_PATH, f'{log_type}.log'), 'rb') as f:
        return sum(1 for _ in f)


def main() -> dict:
    check_net_settings()

    # initialize log directory
    if not os.path.isdir(RENDERED_FILE_PATH):
        os.mkdir(RENDERED_FILE_PATH)

    # clean up prior runs with file truncate
    if os.listdir(RENDERED_FILE_PATH):
        for file_name in os.listdir(RENDERED_FILE_PATH):
            with open(os.path.join(RENDERED_FILE_PATH, file_name), 'wb') as f:
                f.truncate()

    output = {
        'events': {
            'emitted': {},
            'rendered': {},
            'difference': {},
        },
        'udp_stats': {
            'before': {},
            'after': {},
            'difference': {},
        },
        'loss': {},
        'events_per_second': 0,
    }

    # capture udp stats prior to test run
    output['udp_stats']['before'] = {x: y for x, y in get_udp_stats().items() if x in ['InErrors', 'RcvbufErrors']}

    # note time at beginning of run
    dt_begin = time.perf_counter_ns()

    # send events to receiver
    counts = emit_events()

    # capture run duration
    output['duration_seconds'] = (time.perf_counter_ns() - dt_begin) / 1e9  # ns to sec

    # sleep a moment before gathering statistics to allow receiver to finish flushing to disk
    time.sleep(SLEEP_DURATION)

    # capture udp stats after test run
    output['udp_stats']['after'] = {x: y for x, y in get_udp_stats().items() if x in ['InErrors', 'RcvbufErrors']}

    # capture how many logs emitted, rendered on disk, and the difference
    for _idx, example_log in enumerate(EXAMPLE_LOGS):
        destination = example_log.split(' ')[0]
        output['events']['emitted'][destination] = counts[_idx]
        output['events']['rendered'][destination] = count_rendered_logs(destination)
        output['events']['difference'][destination] = output['events']['emitted'][destination] - output['events']['rendered'][destination]

    # calculate udp stats difference
    for key, value in output['udp_stats']['before'].items():
        output['udp_stats']['difference'][key] = output['udp_stats']['after'][key] - value

    # calculate losses
    output['loss']['percent'] = format(sum(output['events']['difference'].values()) / sum(output['events']['emitted'].values()), ".2%")
    output['loss']['total'] = sum(output['events']['difference'].values())

    # calculate rate (events/sec)
    output['events_per_second'] = sum(output['events']['emitted'].values()) / output['duration_seconds']

    return output


if __name__ == '__main__':
    print(json.dumps(main(), indent=4))
