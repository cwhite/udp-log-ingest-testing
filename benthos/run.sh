#!/bin/bash

if [ -z $1 ]; then
  echo 'Usage: ./run.sh config.yaml'
  echo 'Error: config file not provided.'
  exit 1
fi

if [ ! -d '/tmp/rendered_logs' ]; then
  mkdir /tmp/rendered_logs
fi

set -xe

podman run -it --rm --net host \
  -v $(pwd)/demux.py:/usr/local/bin/demux.py:Z \
  -v $(pwd)/$1:/etc/benthos/config.yaml:Z \
  -v /tmp/rendered_logs:/srv/mw-log:Z \
  wikimedia_benthos:latest
